///<reference path="../index.d.ts"/>
var fs = require('fs');
global.Q = require('q');
global._ = require('lodash');
global.moment = require('moment');
var n2hws = require(fs.existsSync('../n2hws/index.js') ? '../n2hws/index' : 'n2hws');
// exports=n2hws;
n2hws.Config.MAX_PROCESS = require('os').cpus().length;
n2hws.Config.MAX_PROCESS = 0;
// n2hws.Config.APP_PATH = __dirname;
// console.log(n2hws.Config.APP_PATH)
n2hws.Config.APP_NAME = "NodeServer";
n2hws.Config.DATABASE = {
    DB_NAME: "print",
    DB_USER: "root",
    DB_PWD: "123456",
    DB_HOST: "127.0.0.1",
    DB_PORT: "3306",
    DB_PREFIX: 'prefix_',
};
n2hws.Config.PUBLIC_PATH = __dirname + '/Public';
n2hws.Config.APP_PORT = 80;
global.M = n2hws.M;
global.Op = n2hws.Op;
global.Sequelize = n2hws.Sequelize;
global.Relation = n2hws.Relation;
global.Controller = n2hws.Controller;
global.EM = n2hws.EM;
global._ = n2hws._;
global.n2hws = n2hws;
global.password_hash = n2hws.password_hash;
EM.on('started', function (host, port) {
    console.log(host);
});
// n2hws.Config.HTTP_ENCODE=function (data, status, error) {
//     return {
//         UID:this.session('UID'),
//         UN:this.session('U'),
//         d:data,
//         err:error,
//         c:status,
//     };
// }
var server = new n2hws.Server();
// n2hws.Server.Redis();
n2hws.Server.run();
