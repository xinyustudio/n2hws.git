class Controller{
    protected $model:String|boolean=false;
    /**
     *
     */
    protected $req;
    /**
     *
     */
    protected $rep;
    /**
     *
     */
    protected $C:string;
    /**
     *
     */
    protected $A:string;
    constructor(req,rep){
        this.$req=req;this.$rep=rep;
        this.$model=this.$model||req.params.C;
        this.$C=req.params.C;this.$A=req.params.A;
    }
    protected _search={
        Keyword:{
            //模糊查询的表名设定，默认为空，禁止Keyword搜索，
            table:"",
            fields:[]
        },
        W:{
            //精确查询的条件限定，默认为空，允许使用所有字段
            table:"",
            fields:[]
        }
    }
    /**
     * session操作
     * @param k
     * @param {any} v
     * @returns {any}
     */
    public session(k,v=undefined) {
        if(k===undefined){
            return this.$req.session
        }else if(k==null){
            this.$req.session.destroy()
        }else if(k){
            if(v){
                this.$req.session[k]=v;
            }else if(v===undefined){
                return this.$req.session?this.$req.session[k]:'';
            }else if(v===null){
                this.$req.session[k]=v;
            }
            return true;
        }
        return undefined;
    };

    /**
     * 发送数据
     * @param d
     */
    public send (d) {
        this.$rep.send(d)
    }

    /**
     * cookie操作
     * @param k
     * @param v
     * @param {number} expire
     */
    public cookie (k,v,expire=3600) {
        this.$rep.cookie(k,v,{
            expires:expire,
        })
    };

    /**
     * 返回成功
     * @param data
     */
    public success (data) {
        this.$rep.send(Config[`${this.$req.from}_ENCODE`].apply(this,[data,200,""]));
    };

    /**
     * 返回错误信息
     * @param tip
     * @param {number} c
     */
    public error (tip,c=500) {
        this.$rep.send(Config[`${this.$req.from}_ENCODE`].apply(this,[{},c,tip]));
    };

    /**
     * 获取对象
     * @param data
     */
    public get (data) {
        var PK= Config.getTablePK(this.$model);
        R(this.$model).where({
            [PK]:this.$req.request(PK,'0')
        }).fields(Config.getTableFields(this.$model)).select().then(d=>{
            this.success(d[0])
        }).catch(e=>{
            this.error('失败')
        })
    };

    /**
     * 获取指定对象数据
     * @param data
     */
    public gets (data) {
        var PK= Config.getTablePK(this.$model);
        var PKs = this.$req.request(`${PK}s`,[]);
        if(!_.isArray(PKs)){
            this.error('查询值不合法')
            return;
        }
        var pass = true;
        _.forOwn(PKs,(v,k)=>{
            if(!_.isNumber(v)){
                pass=false;
            }
        })
        if(!_.isArray(PKs)){
            this.error('查询值不合法')
            return;
        }

        R(this.$model).where({
            [PK]:{
                [Op.in]:this.$req.request(`${PK}s`,[])
            }
        }).fields(Config.getTableFields(this.$model)).select().then(d=>{
            this.success(d[0])
        }).catch(e=>{
            this.error('失败')
        })
    };
    public add () {
        var data = this.$req.body;
        R(this.$model).add(data).then(d=>{
            this.success(d)
        }).catch(e=>{
            this.error(e)
        })
    };

    /**
     * del方法
     */
    public del () {
        var PK= Config.getTablePK(this.$model);
        let PKV = this.$req.request(PK,0);
        if(_.isNumber(PKV)&&PKV>0){
            R(this.$model).where({
                [PK]:PKV
            }).del().then(d=>{
                this.success({
                    [PK]:PKV,
                    Count:d
                })
            }).catch(e=>{
                this.error(e)
            })
        }else{
            this.error('错误的对象编号')
        }
    };
    public save () {
        var PK= Config.getTablePK(this.$model);
        let PKV = this.$req.request(PK,0);
        let Data = this.$req.request('Params',null)
        if(_.isNumber(PKV)&&PKV>0&&_.isObject(Data)){
            R(this.$model).where({
                [PK]:PKV
            }).save(Data).then(d=>{
                this.success(d);
            }).catch(e=>{
                this.error(e)
            })
        }else{
            this.error("错误的对象编号");
        }
    };
    protected render(data:any,template=false){
        var ejs = require('ejs').com;

    }
    public search () {
        var P = this.$req.request('P',1);
        var N = this.$req.request('N',10);
        var W = this.$req.request('W',{});
        var Sort = this.$req.request('Sort',false);
        var Keyword = this.$req.request('Keyword',"");
        var r = R(this.$model)
            .where(Model.parseWhere(W))
            .fields(Config.getTableFields(this.$model))
            .page(P,N)
        if(_.isString(Sort)){
            r.order(Sort)
        }
        if(_.isString(Keyword)&&Keyword.length>0){
            // 遍历允许模糊查询的字段列表，
            if(!_.isArray(this._search.Keyword.fields)){
                if(_.isString(this._search.Keyword.fields)&&this._search.Keyword.fields.length>0){
                    this._search.Keyword.fields=this._search.Keyword.fields.split(',')
                }
            }
            if(_.isArray(this._search.Keyword.fields)){
                var KeywordsWhere={};
                _.forOwn(this._search.Keyword.fields,(v,k)=>{
                    KeywordsWhere[Op.like]=Keyword;
                });
                r.where(KeywordsWhere)
            }
        }
        r.selectAndCount()
            .then(d=>{
                this.success({
                    P,N,L:d[0],T:d[1]
                })
            })
            .catch(e=>{
                this.error('失败')
            })
    };
}
exports.Controller=Controller;