# n2hws框架
基于nodejs的MV/Socket框架
示例项目：https://gitee.com/xinyustudio/n2hws-simple

# 基础准备：
开发工具：PHPStorm/WebStorm  
新建项目，在File=>Setting=>Language中，
Javascript 设置成ECMAScript6
[img(./img/javascript.png)]
NodeJS中开启
TypeScript中开启自动编译

# Relatoin用法
```typescript
let r = new Relation('Goods','GoodsID,ClassID','GoodsID');
r.hasOne({
    name:'Class',
    table:'GoodsClass',
    fields:['ClassID','Title'],
    pk:'ClassID',
    fk:'ClassID'
}).hasMany({
    name:'Stock',
    table:'Stock',
    fields:'GoodsID,Amount,StoreID',
    where:{StoreID:1},
    pk:'GoodsID',
    fk:'GoodsID'
}).extend({
    name:'class',
    table:'GoodsClass',
    fields:['GoodsID',['Title','ClassTitle']],
    pk:'ClassID',
    fk:'ClassID'
}).where({
ClassID:1
}).select().then(r=>{
    this.success(r)
}).catch(err=>{
    this.error(err)
})
```

# PowerDesigner设计稿解析器
```javascript
// 在项目目录下创建Db文件夹
var n2hws = require('n2hws');
var pdm = new (n2hws.PDM);
pdm.parse('D:\\Web\\Node_Printer_Controller\\Doc\\PrintControl.pdm')

```

# 文件上传功能使用方法
文件上传支持

# 功能规划
1、允许根据Host信息切换配置文件，
2、允许用户注册app处理信息

