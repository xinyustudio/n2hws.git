#Relation使用方法
目的：Relation类目标是用于管理数据库对象间的关联关系，目前Relation支持三种关系，分别是一对一，一对多，继承，另外支持对象级别的嵌套
示例Demo：(数据库设计稿见Doc目录下的n2hws_demo.pdm，请使用PowerDesigner打开设计稿)
```SQL
drop table if exists area;

drop table if exists login_log;

drop table if exists user;

drop table if exists user_extend;

/*==============================================================*/
/* Table: area                                                  */
/*==============================================================*/
create table area
(
   AID                  bigint unsigned not null auto_increment,
   Title                char(250) not null,
   primary key (AID)
);

/*==============================================================*/
/* Table: login_log                                             */
/*==============================================================*/
create table login_log
(
   UID                  bigint unsigned not null,
   IP                   char(250) not null,
   primary key (UID)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   UID                  bigint unsigned not null auto_increment,
   AID                  bigint,
   Account              char(250) not null,
   PWD                  char(250) not null,
   IsOpen               boolean not null,
   primary key (UID)
);

/*==============================================================*/
/* Table: user_extend                                           */
/*==============================================================*/
create table user_extend
(
   UID                  bigint unsigned not null,
   Name                 char(250) not null,
   Sex                  boolean not null,
   primary key (UID)
);

alter table login_log add constraint FK_Reference_2 foreign key (UID)
      references user (UID) on delete restrict on update restrict;

alter table user add constraint FK_Reference_3 foreign key (AID)
      references area (AID) on delete restrict on update restrict;

alter table user_extend add constraint FK_Reference_1 foreign key (UID)
      references user (UID) on delete restrict on update restrict;

```
在Relation目录下编写如下代码：
```typescript
///<reference path="../index.d.ts"/>

var User = new Relation('User');
User.hasOne({
    name:'area',
    table:'area',
    fields:'AID,Title',
    pk:'AID',
    fk:'AID'
}).hasMany({
    name:'Log',
    table:'LoginLog',
    fields:'UID,IP',
    pk:'UID',
    fd:'UID'
}).extend({
    name:'extend',
    table:'UserExtend',
    fields:'UID,Name,Sex',
    pk:'UID',
    fd:'UID'
})
exports.User = User;
```
此时使用如下代码调用，返回调用情况，
```typescript
R('User').where({UID:1}).select().then(d=>{
    console.log(d)
    this.success(d)
}).catch(e=>{
    this.error(e)
})
```