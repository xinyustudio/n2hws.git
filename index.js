const path = require('path');
/**
 *
 * @param Table
 * @returns Model
 * @constructor
 */
function M(Table) {
    return new Model(Table, Config.DATABASE.DB_PREFIX);
}
/**
 * 仿造php的array_columns函数
 * @param arr
 * @param column
 * @returns {Array}
 */
function array_columns(arr, column, unique = false) {
    let a = [];
    _.forOwn(arr, (v, k) => {
        if (unique) {
            if (a.indexOf(v[column]) == -1) {
                a.push(v[column]);
            }
        }
        else {
            a.push(v[column]);
        }
    });
    return a;
}
/**
 * 取唯一值
 * @param arr
 */
function array_unique(arr) {
    return _.unique(arr);
}
/**
 *
 * @param name
 * @param {boolean} force
 * @returns Relation
 * @constructor
 */
function R(name, force = false) {
    const Relations = {};
    if (!_.isObject(Relations[name])) {
        try {
            var r = require(Config.APP_PATH + '/app/Relation.js');
            if (r && r[name]) {
                Relations[name] = r[name];
            }
            else {
                Relations[name] = new Relation(name, Config.getTableFields(name), Config.getTablePK(name));
            }
        }
        catch (e) {
            Relations[name] = new Relation(name, Config.getTableFields(name), Config.getTablePK(name));
        }
    }
    return Relations[name];
}
exports.array_columns = array_columns;
exports.array_unique = array_unique;
/**
 * 密码加密
 * @param pwd
 * @returns {string}
 */
function password_hash(pwd) {
    const md5 = crypto.createHash('md5');
    return md5.update(pwd).digest('hex').toLowerCase();
}
exports.password_hash = password_hash;
/**
 * 密码验证
 * @param pwd
 * @param sign
 * @returns {boolean}
 */
function password_verify(pwd, sign) {
    const md5 = crypto.createHash('md5');
    return md5.update(pwd).digest('hex').toLowerCase() == sign;
}
exports.password_verify = password_verify;
exports.M = M;
exports.R = R;
/**
 * 移动上传文件到指定地址
 * @param file
 * @param to
 */
var move_upload_file = module.exports.move_upload_file = function (file, to) {
    fs.renameSync(file.path, to);
};
/**
 * 接受用户上传的所有文件，
 * @param req
 * @returns {any[]}
 */
var accept_upload_file = module.exports.accept_upload_file = function (req) {
    var files = [];
    _.forOwn(req.files, (file) => {
        let d = file.path.substr(0, file.path.length - 4);
        files.push(d);
        move_upload_file(file, d);
    });
    return files;
};
// 创建所有目录
var mkdirs = module.exports.mkdirs = function (dirpath, mode = 0o777, callback = null) {
    if (fs.existsSync(dirpath)) {
        return true;
    }
    else {
        let parent_path = path.dirname(dirpath);
        if (fs.existsSync(parent_path)) {
            return fs.mkdirSync(dirpath);
        }
        else {
            return mkdirs(parent_path);
        }
    }
};
///<reference path="Functions.ts"/>
class Config {
    /**
     * 获取表的定义
     * @param ModelName
     * @returns {{}}
     */
    static getTableDefine(ModelName) {
        var config = {};
        try {
            var s = require(Config.APP_PATH + `/Db/${ModelName}.js`);
            if (s) {
                config = s[ModelName];
            }
        }
        catch (e) {
        }
        finally {
            return config;
        }
    }
    /**
     * 获取表的主键名称
     * @param ModelName
     * @returns {string}
     */
    static getTablePK(ModelName) {
        var PK = 'ID';
        _.forOwn(Config.getTableDefine(ModelName), (v, k) => {
            if (v.primaryKey === true) {
                PK = k;
            }
        });
        return PK;
    }
    /**
     * 获取表字段列表
     * @param ModelName
     * @returns {any[]}
     */
    static getTableFields(ModelName) {
        var Fields = [];
        _.forOwn(Config.getTableDefine(ModelName), (v, k) => {
            Fields.push(k);
        });
        return Fields;
    }
}
/**
 * 是否开启转发，转发目标地址是什么
 * @type {boolean|string}
 */
Config.PROXY_PASS = false;
Config.PROXY_DECODE = function (response, body) {
    return body;
};
Config.PROXY_ENCODE = function (req, rep) {
    return req.body;
};
Config.SESSION_PATH = require('os').type() == "Windows_NT" ? "./session" : "/tmp";
/**
 * 静态目录地址
 * @type {string}
 */
Config.PUBLIC_PATH = __dirname + '/Public';
/**
 * 是否开启调试模式
 * @type {boolean}
 */
Config.DEBUG = true;
/**
 * 是否启用日志，
 * @type {boolean}
 */
Config.LOGER = false;
/**
 * 来自Redis的请求
 * @type {{host: string; port: number; password: string; db: number; request: string; response: string}}
 */
Config.FROM_REDIS = {
    host: '127.0.0.1',
    port: 6379,
    password: '',
    db: 0,
    request: 'request',
    response: 'response' //响应频道
};
Config.REDIS_DECODE = function (data, channel) {
    var d = {
        i: 'Error/_404',
        d: {},
        t: '',
        s: '' //session_id
    };
    try {
        if (_.isString(data)) {
            data = JSON.parse(data);
            if (_.isObject(data)) {
                d.i = data.i || 'Error/_404';
                d.d = data.d;
                d.t = data.t;
                d.s = data.s || "";
            }
        }
        else {
        }
    }
    catch (e) {
    }
    return d;
};
Config.REDIS_ENCODE = function (data, status = 200, error = "") {
    return {
        g: this.session ? this.session('g') || [0] : [0],
        u: this.session ? this.session('UID') || "" : '',
        i: this.$req.query.i,
        c: status,
        d: data,
        e: error
    };
};
Config.HTTP_DECODE = function (data) {
    var d = {
        i: 'Error/_404',
        d: {},
        t: '',
        s: '' //session_id
    };
    try {
        if (_.isString(data)) {
            data = JSON.parse(data);
            if (_.isObject(data)) {
                d.i = data.i || 'Error/_404';
                d.d = data.d;
                d.t = data.t;
                d.s = data.s || "";
            }
        }
        else {
        }
    }
    catch (e) {
    }
    return d;
};
Config.HTTP_ENCODE = function (data, status = 200, error = "") {
    return {
        g: this.session ? this.session('g') || [0] : [0],
        u: this.session ? this.session('UID') || "" : '',
        i: this.$req.query.i,
        c: status,
        d: data,
        e: error
    };
};
Config.WS_DECODE = function (data) {
    var d = {
        i: 'Error/_404',
        d: {},
        t: '',
        s: '' //session_id
    };
    try {
        if (_.isString(data)) {
            data = JSON.parse(data);
            if (_.isObject(data)) {
                d.i = data.i || 'Error/_404';
                d.d = data.d;
                d.t = data.t;
                d.s = data.s || "";
            }
        }
        else {
        }
    }
    catch (e) {
    }
    return d;
};
Config.WS_ENCODE = function (data, status = 200, error = "") {
    return {
        g: this.session ? this.session('g') || [0] : [0],
        u: this.session ? this.session('UID') || "" : '',
        i: this.$req.query.i,
        c: status,
        d: data,
        e: error
    };
};
/**
 * 模型定义路径
 * @type {string}
 */
Config.MODEL_PATH = "";
/**
 * 应用名称
 * @type {string}
 */
Config.APP_NAME = "N2hwsSimple";
/**
 * session处理方式
 * @type {string}
 */
Config.SESSION_HANDLER = "file";
/**
 * 最大进程数
 * @type {number}
 */
Config.MAX_PROCESS = require('os').cpus().length;
/**
 * 应用目录
 * @type {string}
 */
Config.APP_PATH = process.cwd();
Config.TMP_PATH = [process.cwd(), 'Tmp'].join(path.sep);
Config.UPLOAD_PATH = [process.cwd(), 'Upload'].join(path.sep);
Config.APP_DEBUG = false;
/**
 * 监听端口
 * @type {number}
 */
Config.APP_PORT = 18080;
/**
 * 监听
 * @type {string}
 */
Config.APP_HOST = "0.0.0.0";
/**
 * Redis配置
 * @type {{host: string; port: string; pass: string; db: number; ttl: number; logErrors: boolean}}
 */
Config.SESSION_REDIS = {
    "host": "127.0.0.1",
    "port": "6379",
    "pass": "",
    "db": 1,
    "ttl": 1800,
    "logErrors": true
};
/**
 * 数据库配置
 * @type {{DB_NAME: string; DB_USER: string; DB_PWD: string; DB_HOST: string; DB_PORT: string; DB_PREFIX: string}}
 */
Config.DATABASE = {
    DB_NAME: "test",
    DB_USER: "root",
    DB_PWD: "123456",
    DB_HOST: "localhost",
    DB_PORT: "3306",
    DB_PREFIX: 'prefix_',
};
exports.Config = Config;
class Controller {
    constructor(req, rep) {
        this.$model = false;
        this._search = {
            Keyword: {
                //模糊查询的表名设定，默认为空，禁止Keyword搜索，
                table: "",
                fields: []
            },
            W: {
                //精确查询的条件限定，默认为空，允许使用所有字段
                table: "",
                fields: []
            }
        };
        this.$req = req;
        this.$rep = rep;
        this.$model = this.$model || req.params.C;
        this.$C = req.params.C;
        this.$A = req.params.A;
    }
    /**
     * session操作
     * @param k
     * @param {any} v
     * @returns {any}
     */
    session(k, v = undefined) {
        if (k === undefined) {
            return this.$req.session;
        }
        else if (k == null) {
            this.$req.session.destroy();
        }
        else if (k) {
            if (v) {
                this.$req.session[k] = v;
            }
            else if (v === undefined) {
                return this.$req.session ? this.$req.session[k] : '';
            }
            else if (v === null) {
                this.$req.session[k] = v;
            }
            return true;
        }
        return undefined;
    }
    ;
    /**
     * 发送数据
     * @param d
     */
    send(d) {
        this.$rep.send(d);
    }
    /**
     * cookie操作
     * @param k
     * @param v
     * @param {number} expire
     */
    cookie(k, v, expire = 3600) {
        this.$rep.cookie(k, v, {
            expires: expire,
        });
    }
    ;
    /**
     * 返回成功
     * @param data
     */
    success(data) {
        this.$rep.send(Config[`${this.$req.from}_ENCODE`].apply(this, [data, 200, ""]));
    }
    ;
    /**
     * 返回错误信息
     * @param tip
     * @param {number} c
     */
    error(tip, c = 500) {
        this.$rep.send(Config[`${this.$req.from}_ENCODE`].apply(this, [{}, c, tip]));
    }
    ;
    /**
     * 获取对象
     * @param data
     */
    get(data) {
        var PK = Config.getTablePK(this.$model);
        R(this.$model).where({
            [PK]: this.$req.request(PK, '0')
        }).fields(Config.getTableFields(this.$model)).select().then(d => {
            this.success(d[0]);
        }).catch(e => {
            this.error('失败');
        });
    }
    ;
    /**
     * 获取指定对象数据
     * @param data
     */
    gets(data) {
        var PK = Config.getTablePK(this.$model);
        var PKs = this.$req.request(`${PK}s`, []);
        if (!_.isArray(PKs)) {
            this.error('查询值不合法');
            return;
        }
        var pass = true;
        _.forOwn(PKs, (v, k) => {
            if (!_.isNumber(v)) {
                pass = false;
            }
        });
        if (!_.isArray(PKs)) {
            this.error('查询值不合法');
            return;
        }
        R(this.$model).where({
            [PK]: {
                [Op.in]: this.$req.request(`${PK}s`, [])
            }
        }).fields(Config.getTableFields(this.$model)).select().then(d => {
            this.success(d[0]);
        }).catch(e => {
            this.error('失败');
        });
    }
    ;
    add() {
        var data = this.$req.body;
        R(this.$model).add(data).then(d => {
            this.success(d);
        }).catch(e => {
            this.error(e);
        });
    }
    ;
    /**
     * del方法
     */
    del() {
        var PK = Config.getTablePK(this.$model);
        let PKV = this.$req.request(PK, 0);
        if (_.isNumber(PKV) && PKV > 0) {
            R(this.$model).where({
                [PK]: PKV
            }).del().then(d => {
                this.success({
                    [PK]: PKV,
                    Count: d
                });
            }).catch(e => {
                this.error(e);
            });
        }
        else {
            this.error('错误的对象编号');
        }
    }
    ;
    save() {
        var PK = Config.getTablePK(this.$model);
        let PKV = this.$req.request(PK, 0);
        let Data = this.$req.request('Params', null);
        if (_.isNumber(PKV) && PKV > 0 && _.isObject(Data)) {
            R(this.$model).where({
                [PK]: PKV
            }).save(Data).then(d => {
                this.success(d);
            }).catch(e => {
                this.error(e);
            });
        }
        else {
            this.error("错误的对象编号");
        }
    }
    ;
    render(data, template = false) {
        var ejs = require('ejs').com;
    }
    search() {
        var P = this.$req.request('P', 1);
        var N = this.$req.request('N', 10);
        var W = this.$req.request('W', {});
        var Sort = this.$req.request('Sort', false);
        var Keyword = this.$req.request('Keyword', "");
        var r = R(this.$model)
            .where(Model.parseWhere(W))
            .fields(Config.getTableFields(this.$model))
            .page(P, N);
        if (_.isString(Sort)) {
            r.order(Sort);
        }
        if (_.isString(Keyword) && Keyword.length > 0) {
            // 遍历允许模糊查询的字段列表，
            if (!_.isArray(this._search.Keyword.fields)) {
                if (_.isString(this._search.Keyword.fields) && this._search.Keyword.fields.length > 0) {
                    this._search.Keyword.fields = this._search.Keyword.fields.split(',');
                }
            }
            if (_.isArray(this._search.Keyword.fields)) {
                var KeywordsWhere = {};
                _.forOwn(this._search.Keyword.fields, (v, k) => {
                    KeywordsWhere[Op.like] = Keyword;
                });
                r.where(KeywordsWhere);
            }
        }
        r.selectAndCount()
            .then(d => {
            this.success({
                P, N, L: d[0], T: d[1]
            });
        })
            .catch(e => {
            this.error('失败');
        });
    }
    ;
}
exports.Controller = Controller;
exports.Init = class Init {
    static init(APP_PATH) {
        fs.createReadStream(module.filename.replace('index.js', 'tmp.zip'))
            .pipe(require('unzip').Extract({
            path: APP_PATH
        }));
        fs.copyFileSync('node_modules/n2hws/index.d.ts', `${APP_PATH}/Server/index.d.ts`);
    }
    static create() {
        let archiver = require('archiver');
        var output = fs.createWriteStream();
    }
};
class Model {
    /**
     * 构造器
     * @param Table
     * @param {string} Prefix
     */
    constructor(Table, Prefix = "") {
        this._true_table_name = "";
        this._fields = [];
        this._table_name = "";
        this._options = {
            fields: [], where: {}
        };
        this._config = {};
        this.$search = {};
        /**
         * 解析查询条件
         * @returns {{} | any}
         * @private
         */
        this._parse_where = function () {
            return this._options.where;
        };
        /**
         * 获取查询字段
         * @returns {Array}
         * @private
         */
        this._parse_fields = function () {
            if (this._options.fields.length == 0) {
                return Config.getTableFields(this._table_name);
            }
            return this._options.fields;
        };
        /**
         * 解析配置文件，生成查询属性
         * @returns {{}}
         * @private
         */
        this._parse_config = function () {
            let config = {};
            config['attributes'] = this._parse_fields();
            config['where'] = this._parse_where();
            if (this._options.order) {
                config['order'] = this._parse_order();
            }
            if (this._options.limit) {
                config['limit'] = this._options.limit;
            }
            if (this._options.offset) {
                config['offset'] = this._options.offset;
            }
            return config;
        };
        /**
         * 设置表的字段，默认读取所有的
         * @param fields
         */
        this.setTableFields = function (fields) {
            this._fields = fields;
        };
        this.define = function (config = {}) {
            //TODO 加载数据库表结构定义
            try {
                let table = require(Config.APP_PATH + `/Db/${this._table_name}.js`);
                if (table && table[this._table_name]) {
                    config = table[this._table_name];
                }
            }
            catch (e) {
                console.log(e);
            }
            return config;
        };
        /**
         * 获取一个Sequelize的模型
         * @returns {any}
         */
        this.getModel = function () {
            if (db == null) {
                db = new Sequelize(Config.DATABASE.DB_NAME || "test", Config.DATABASE.DB_USER || "root", Config.DATABASE.DB_PWD || "", {
                    host: Config.DATABASE.DB_HOST || "localhost",
                    port: Config.DATABASE.DB_PORT || 3306,
                    dialect: "mysql",
                    operatorsAliases: false,
                    pool: {
                        max: 2,
                        min: 1,
                        idle: 10000
                    }
                });
            }
            if (!this._model) {
                this._model = db.models[this._true_table_name] ? db.models[this._true_table_name] : db.define(this._true_table_name, this.define(), {
                    freezeTableName: true,
                    timestamps: false
                });
            }
            return this._model;
        };
        this.object = function () {
        };
        /**
         * 设定Where条件
         * @param where
         * @returns {Model.where}
         */
        this.where = function (where) {
            if (null == where) {
                this._options.where = {};
            }
            else if (_.isObject(where))
                this._options.where = Object.assign(this._options.where, where);
            else {
                console.log('Error Where', where);
            }
            return this;
        };
        /**
         * 设定字段列表，支持数组和字符串格式
         * @param {Number | String} fields
         * @param {boolean} exclude
         * @returns {Model.fields}
         */
        this.fields = function (fields, exclude = false) {
            if (_.isArray(fields)) {
                this._options.fields = _.concat(this._options.fields, fields);
            }
            else if (_.isString(fields)) {
                this._options.fields = _.concat(this._options.fields, fields.split(','));
            }
            return this;
        };
        /**
         * 设定排序规则，
         * @param {String} order
         * @returns {Model.order}
         */
        this.order = function (order) {
            this._options.order = order;
            return this;
        };
        /**
         * 发起查询请求
         * @returns {Bluebird<any[]>}
         */
        this.select = function () {
            return this.getModel().findAll(this._parse_config()).then(d => {
                let data = [];
                this._clean();
                d.forEach(v => {
                    data.push(v.dataValues);
                });
                return data;
            });
        };
        this.add = function (data) {
            return this.getModel().create(data).then(d => {
                this._clean();
                return d.dataValues;
            });
        };
        this.data = function (data) {
            this.getModel().build(data);
            return this;
        };
        /**
         * 查找一个
         */
        this.find = function () {
            return this.limit(1).select().then(d => {
                this._clean();
                return d[0];
            });
        };
        /**
         * 批量添加数据
         * @param data
         * @returns {any}
         */
        this.addAll = function (data) {
            return this.getModel().bulkCreate(data, {
                fields: Object.keys(data[0])
            }).then(d => {
                this._clean();
                let data = [];
                d.forEach(v => {
                    data.push(v.dataValues);
                });
                return data;
            });
        };
        /**
         * 取数量
         */
        this.count = function () {
        };
        /**
         * 支持selectAndCount
         * @returns {Promise<{count; rows: any[]}>}
         */
        this.selectAndCount = function () {
            return this.getModel().findAndCountAll(this._parse_config()).then(d => {
                let data = [];
                d.rows.forEach(v => {
                    data.push(v.dataValues);
                });
                this._clean();
                return {
                    count: d.count,
                    rows: data
                };
            });
        };
        /**
         * 设置limit参数，
         * @param {number} Number
         */
        this.limit = function (Number) {
            this._options.limit = Number;
            return this;
        };
        /**
         * 设置分页参数
         * @param {number} Page
         * @param {number} Number
         * @returns {Model.page}
         */
        this.page = function (p, n) {
            this._options.limit = Number(n);
            this._options.offset = (p - 1) * n;
            return this;
        };
        /**
         * 调用delete语句
         * @returns {any}
         */
        this.del = function () {
            return this.getModel().destroy({
                where: this._parse_where(this._options.where)
            }).then(d => {
                this._clean();
                return d;
            });
        };
        /**
         * 调用save方法
         * @param data
         */
        this.save = function (data) {
            return this.getModel().update(data, {
                where: this._parse_where(this._options.where),
                options: {
                    returning: true
                }
            }).then(d => {
                this._clean();
                return d;
            });
        };
        /**
         * 执行自定义请求
         * @param sql
         * @returns {any}
         */
        this.query = function (sql) {
            return db.query(sql.replace(/__DB_PREFIX__/g, Config.DATABASE.DB_PREFIX), {
                type: Sequelize.QueryTypes.SELECT
            });
        };
        this.exec = function (SQL, Type) {
            return db.query(SQL.replace(/__DB_PREFIX__/g, Config.DATABASE.DB_PREFIX, { type: Type }));
        };
        /**
         * 开启事物
         * @returns {IDBTransaction | any}
         */
        this.startTrans = function () {
            return this.getModel().transaction();
        };
        /**
         * 提交
         */
        this.commit = function () {
        };
        /**
         * 回滚
         */
        this.rollback = function () {
        };
        /**
         * 清除查询条件，
         * @private
         */
        this._clean = function () {
            this._options.fields = [];
            this._options.where = {};
            if (!_.isUndefined(this._options.limit))
                delete this._options.limit;
            if (!_.isUndefined(this._options.offset))
                delete this._options.offset;
        };
        this._table_name = Table;
        this._true_table_name = Prefix + Table.replace(/([A-Z])/g, function ($0, $1) {
            return '_' + $1.toLowerCase();
        }).replace(/_(.+)/, "$1");
        this._db = db;
    }
    static parseWhere(where) {
        var w = {};
        _.forOwn(where, (v, k) => {
            if (_.isObject(v)) {
                w[k] = {};
                _.forOwn(v, (o, p) => {
                    p = p.toLowerCase();
                    if (!_.isUndefined(Op[p])) {
                        w[k][Op[p]] = o;
                    }
                    else {
                        w[k][p] = o;
                    }
                });
            }
            else {
                w[k] = v;
            }
        });
        return w;
    }
    get config() {
        return this._config;
    }
    set config(config) {
        this._config = config;
    }
    _parse_order() {
        var order = [];
        if ('string' == typeof this._options.order) {
            this._options.order = this._options.order.split(',');
        }
        if (_.isArray(this._options.order)) {
            _.forOwn(this._options.order, (v, k) => {
                order.push(v.split(' '));
            });
        }
        return order;
    }
    /**
     * 检测是否存在并在不存在的情况下添加数据
     * @param data
     */
    addIfNotExist(data, where = null) {
        return this.where(where || data).find().then(d => {
            if (_.isObject(d)) {
                //存在
                return true;
            }
            else {
                //不存在
                return this.add(data);
            }
        });
    }
    /**
     * 获取某个字段
     * @param Fields
     * @param {boolean} More
     * @returns {any}
     */
    getFields(Fields, More = false) {
        if (!More) {
            this.page(1, 1);
        }
        if (_.isString(Fields)) {
            Fields = Fields.split(',');
        }
        if (Fields.length > 0)
            return this.fields(Fields).select().then(d => {
                this._clean();
                var pk = Fields[0];
                if (d.length > 0) {
                    if (More) {
                        var data = {};
                        var odata = [];
                        _.forOwn(d, (v, k) => {
                            if (Fields.length == 1) {
                                odata.push(v[pk]);
                            }
                            else {
                                data[v[pk]] = v;
                            }
                        });
                        return Fields.length == 1 ? odata : data;
                    }
                    else {
                        if (Fields.length == 1) {
                            return d[0][pk];
                        }
                        else {
                            return d[0];
                        }
                    }
                }
                return "";
            });
        else {
            return Q({});
        }
    }
}
exports.Model = Model;
exports.PDM = class PDM {
    constructor() {
        this._data = {};
        this._tables = {};
        this._IDMap = {};
        this._domain = {};
        this._json = {};
    }
    parse(path) {
        var xmlparse = require('xml2js').parseString;
        var fs = require('fs');
        var content = fs.readFileSync(path);
        xmlparse(content, (err, result) => {
            if (err === null) {
                this._json = result.Model["o:RootObject"]["0"]["c:Children"]["0"]["o:Model"]["0"];
                this.parse_domain();
                this.parse_fk();
                this.parse_table();
                this.gen_db_js();
                this.gen_relation();
                this.gen_controller();
                process.exit();
            }
        });
    }
    parse_fk() {
    }
    parse_domain() {
        _.forOwn(this._json["c:Domains"]["0"]["o:PhysicalDomain"], v => {
            this._domain[v.$.Id] = {
                Code: v['a:Code'][0],
                Name: v['a:Name'][0],
                Must: v["a:PhysicalDomain.Mandatory"] ? v["a:PhysicalDomain.Mandatory"]["0"] == 1 : false,
                Identity: v["a:Identity"] ? v["a:Identity"][0] == 1 : false,
                DataType: v["a:DataType"] ? v["a:DataType"][0] == 1 : false,
                Id: v.$.Id
            };
        });
    }
    parse_table() {
        var DefaultValueMap = {
            "\"\"": ''
        };
        var tables = {};
        _.forOwn(this._json['c:Tables'][0]['o:Table'], (v, k) => {
            var code = v['a:Code'][0].replace('prefix_', '');
            var table = {
                Name: v['a:Name'][0],
                Code: code.replace(/(\w+[_\w]{0,})/g, ($0, $1, $2) => {
                    // console.log($0)
                    return $1.replace(/_(\w)/g, ($a, $b) => {
                        return $b.toUpperCase();
                    }).replace(/(\w)/, ($c, $d) => {
                        return $d.toUpperCase();
                    });
                }),
                Columns: [],
                FKs: [],
            };
            var pkid = false;
            var tpkid = false;
            if (v["c:PrimaryKey"]
                && v["c:PrimaryKey"]["0"]
                && v["c:PrimaryKey"]["0"]["o:Key"]
                && v["c:PrimaryKey"]["0"]["o:Key"]["0"]) {
                pkid = v["c:PrimaryKey"]["0"]["o:Key"]["0"].$.Ref;
                _.forOwn(v["c:Keys"]["0"]["o:Key"], (key) => {
                    if (key.$.Id == pkid) {
                        tpkid = key["c:Key.Columns"]["0"]["o:Column"]["0"].$.Ref;
                    }
                });
            }
            else {
                console.error(`表 ${v['a:Name'][0]} 的 主键 未设定`);
            }
            pkid = tpkid;
            if (v['c:Columns']
                && v['c:Columns'][0]
                && v['c:Columns'][0]['o:Column']) {
                _.forOwn(v['c:Columns'][0]['o:Column'], (c, i) => {
                    table.Columns.push({
                        Name: c['a:Name'][0],
                        Comment: c["a:Comment"] ? c["a:Comment"]["0"] : '',
                        Code: c['a:Code'][0],
                        Domain: c["c:Domain"] ? this._domain[c["c:Domain"]["0"]["o:PhysicalDomain"]["0"].$.Ref] : {},
                        DataType: c['a:DataType'][0],
                        Identity: c['a:Identity'] == 1,
                        PrimaryKey: pkid == c.$.Id,
                        Unsigned: c["a:ExtendedAttributesText"] && c["a:ExtendedAttributesText"]["0"] ? c["a:ExtendedAttributesText"]["0"].indexOf('Unsigned') > -1 : false,
                        Must: c["a:Column.Mandatory"] ? c["a:Column.Mandatory"]["0"] == 1 : false,
                        Default: c["a:DefaultValue"] ? (DefaultValueMap[c["a:DefaultValue"]["0"]] === undefined ? c["a:DefaultValue"]["0"] : DefaultValueMap[c["a:DefaultValue"]["0"]]) : ''
                    });
                });
            }
            else {
                console.error(`表 ${v['a:Name'][0]} 的 字段 未设定`);
            }
            tables[table.Code] = table;
        });
        this._tables = tables;
    }
    gen_db_js() {
        console.log('Write Db JS Start');
        // 生成Db下的js文件
        var fs = require('fs');
        if (!fs.existsSync('Db')) {
            fs.mkdirSync('Db');
        }
        _.forOwn(this._tables, (table, name) => {
            var columns = [];
            _.forOwn(table.Columns, (column) => {
                columns.push(`//${column.Name} ${column.Comment.replace("\r\n", "").replace("\r", "").replace("\n", "")}
    ${column.Code}:{
        type:type.${this.get_type(column.DataType.toUpperCase())},
        primaryKey:${column.PrimaryKey},
        autoIncrement:${column.Identity},
        defaultValue:${this.get_defaultValue(column.Default)},
        allowNull:${!column.Must}
    }`);
            });
            var js = `var type = require('sequelize')
exports.${name}={
    ${columns.join(',\r\n   ')}
}`;
            let path = `Db/${name}.js`;
            fs.writeFileSync(path, js);
            console.log(`Write Db/${name}.js`);
        });
        console.log(`Write Db Success`);
    }
    get_type(type) {
        if (type.indexOf('INT(') > -1) {
            return 'BIGINT';
        }
        else if (type.indexOf('VARCHAR(') > -1) {
            return 'STRING';
        }
        if (type == "DATETIME") {
            return "DATE";
        }
        else {
            return type;
        }
    }
    get_defaultValue(value) {
        switch (value) {
            case 'CURRENT_TIMESTAMP':
                return 'type.NOW';
            case "NULL":
                return "null";
            default:
                return value.length == 0 ? "''" : value;
        }
    }
    gen_relation() {
        //TODO 生成Relation类
        console.log(`Write Relation Start`);
        var dir = 'Server/Relation';
        var fs = require('fs');
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        _.forOwn(this._tables, (table, name) => {
            var columns = [];
            _.forOwn(table.Columns, (column) => {
                columns.push(`//${column.Name} ${column.Code} ${column.DataType.toUpperCase()} ${column.Comment.replace("\r\n", "").replace("\r", "").replace("\n", "")}`);
            });
            var js = `//${table.Name}
${columns.join(',\r\n   ')}
var ${name}Relation = new Relation("${name}");
exports.${name}Relation=${name}Relation;`;
            let path = `${dir}/${name}Relation.ts`;
            fs.writeFileSync(path, js);
            console.log(`Write Server/Relation/${name}Relation.ts`);
        });
        console.log(`Write Relation Success`);
    }
    gen_controller() {
        console.log(`Write Controller Start`);
        var dir = 'Server/Controller';
        var fs = require('fs');
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        _.forOwn(this._tables, (table, name) => {
            var columns = [];
            _.forOwn(table.Columns, (column) => {
                columns.push(`//${column.Name} ${column.Code} ${column.DataType.toUpperCase()} ${column.Comment.replace("\r\n", "").replace("\r", "").replace("\n", "")}`);
            });
            var js = `//${table.Name}
${columns.join(',\r\n   ')}
class ${name}Controller extends Controller{

}
exports.${name}Controller=${name}Controller;`;
            let path = `${dir}/${name}Controller.ts`;
            fs.writeFileSync(path, js);
            console.log(`Write Server/Controller/${name}Controller.ts`);
        });
        console.log(`Write Controller Success`);
    }
};
class Redis {
    constructor(Host = "127.0.0.1", Port = 6379, PWD = false, DB = 0) {
        this._sub = {};
        this._channels = [];
        /**
         * 发布
         * @param channel
         * @param message
         */
        this.publish = function (channel, message) {
            this._redis.publish(channel, "string" == typeof message ? message : JSON.stringify(message));
        };
        /**
         * 订阅
         * @param channel
         * @param {Function} cb
         */
        this.subscribe = function (channel) {
            if (!this._sub[channel]) {
                let redis = require('ioredis');
                this._sub[channel] = new redis(this._config);
            }
            this._sub[channel].subscribe(channel, function (err, count) {
            });
            this._sub[channel].on('message', (pattern, channel, message) => {
                try {
                    var that = this;
                    let json = JSON.parse(message);
                    if (json.i && json.i.length > 2) {
                        let C, A, i = json.i.split('/');
                        if (i.length == 2) {
                            C = i[0];
                            A = i[1];
                            if ("string" === typeof json.d && json.d.length > 0) {
                                json.d = JSON.parse(json.d);
                            }
                            let req = {
                                body: json.d,
                                query: {
                                    A: A, C: C,
                                },
                                params: {
                                    A: A, C: C,
                                },
                                session: {
                                    destroy: function () { }
                                }
                            }, rep = {
                                send: function (data, status = 200) {
                                    that._redis.publish("r_" + channel, {
                                        i: json.i,
                                        t: json.t,
                                        d: JSON.stringify(data),
                                        c: status
                                    });
                                }
                            };
                            Server.onRequest(req, rep, function () {
                            });
                        }
                    }
                }
                catch (e) {
                }
            });
        };
        /**
         * redis从获取请求数据
         * @param channel
         */
        this.command = function (channel) {
            this.subscribe(channel, function (err, count) {
            });
        };
        let redis = require('ioredis');
        this._config = {
            host: Host, port: Port,
            db: DB,
        };
        if ("string" == typeof PWD && PWD.length > 0) {
            this._config.password = PWD;
        }
        this._redis = new redis(this._config);
    }
    ;
}
exports.Redis = Redis;
/**
 * Created by castle on 2017-11-24.
 */
/**
 * 关系设定类
 */
class Relation {
    /**
     *
     * @param Table 表名
     * @param Fields 字段列表
     * @param PK 主键
     */
    constructor(Table, Fields = "", PK = "") {
        this._one = {};
        this._many = {};
        this._extend = {};
        this._table = "";
        this._pk = "";
        this._fields = [];
        this._foreach = [];
        this._table = Table;
        let _fields = [];
        if (_.isString(Fields)) {
            _fields = Fields.split(',');
        }
        if (Fields.length == 0 || PK.length == 0) {
            _fields = Config.getTableFields(Table);
            PK = Config.getTablePK(Table);
        }
        this._fields = _fields;
        this._pk = PK;
        this._model = M(Table);
    }
    /**
     * 可以不通过构造函数来添加属性信息
     * @param Table
     * @param Fields
     * @param PK
     */
    config(Table, Fields, PK) {
        this._table = Table;
        this._fields = Fields;
        this._pk = PK;
        if (Table)
            this._model = M(Table);
    }
    /**
     * 拥有一个
     * @param name 关系名称
     * @param Table 表名称
     * @param Fields 要取的字段范围
     * @param TableField 子表字段
     * @param MainField 主表字段
     */
    hasOne(config) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._one = Object.assign(this._one, { [config.name]: {
                    name: '',
                    table: '',
                    fields: [],
                    pk: '',
                    fk: ''
                } }, { [config.name]: config });
        }
        return this;
    }
    get One() {
        return this._one;
    }
    set One(config) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._one = Object.assign(this._one, { [config.name]: {
                    name: '',
                    table: config.name,
                    fields: [],
                    pk: this._pk,
                    fk: this._pk,
                    relation: false
                } }, { [config.name]: config });
        }
    }
    /**
     * 有多个配置
     * @param config
     * @returns {boolean}
     */
    hasMany(config) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._many = Object.assign(this._many, { [config.name]: {
                    name: '',
                    table: config.name,
                    fields: [],
                    pk: this._pk,
                    fk: this._pk,
                    relation: false
                } }, { [config.name]: config });
        }
        return this;
    }
    get Many() {
        return this._many;
    }
    set Many(config) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._many = Object.assign(this._many, { [config.name]: {
                    name: '',
                    table: config.name,
                    fields: [],
                    pk: this._pk,
                    fk: this._pk,
                    relation: false
                } }, { [config.name]: config });
        }
    }
    /**
     * 扩展字段配置
     * @param config
     * @returns {boolean}
     */
    extend(config) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._extend = Object.assign(this._extend, { [config.name]: {
                    name: '',
                    table: config.name,
                    fields: [],
                    pk: this._pk,
                    fk: this._pk,
                    relation: false
                } }, { [config.name]: config });
        }
        return this;
    }
    /**
     *
     * @returns {any}
     * @constructor
     */
    get Extend() {
        return this._extend;
    }
    /**
     *
     * @param config
     * @constructor
     */
    set Extend(config) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._extend = Object.assign(this._extend, { [config.name]: {
                    name: '',
                    table: config.name,
                    fields: [],
                    pk: this._pk,
                    fk: this._pk
                } }, { [config.name]: config });
        }
    }
    // set Fields(fields){
    //     this._fields=fields;
    // }
    // get Fields(){
    //     return this._fields;
    // }
    /**
     * 获取对象
     * @param {Array<Number>} PKValues
     * @returns {any}
     */
    objects(PKValues) {
        if (_.isArray(PKValues)) {
            return this._model.fields(this._fields).where({
                [this._pk]: PKValues
            }).select().then(data => {
                //开始循环属性配置并生成相关。。
                let Qs = [data];
                if (_.isArray(data)) {
                    // data = JSON.parse(JSON.stringify(data))
                    _.forOwn(this._one, (v, k) => {
                        if (!_.isString(v.relation)) {
                            Qs.push(M(v.table).fields(_.isFunction(v.fields) ? v.fields() : v.fields).where(v.where).where({ [v.fk]: array_columns(data, v.pk, true) }).select());
                        }
                        else {
                            Qs.push(R(v.relation).fields(_.isFunction(v.fields) ? v.fields() : v.fields).where(v.where).where({ [v.fk]: array_columns(data, v.pk, true) }).select());
                        }
                    });
                    _.forOwn(this._many, (v, k) => {
                        if (!_.isString(v.relation)) {
                            Qs.push(M(v.table).fields(_.isFunction(v.fields) ? v.fields() : v.fields).where(v.where).where({ [v.fk]: array_columns(data, v.pk, true) }).select());
                        }
                        else {
                            Qs.push(R(v.relation).fields(_.isFunction(v.fields) ? v.fields() : v.fields).where(v.where).where({ [v.fk]: array_columns(data, v.pk, true) }).select());
                        }
                    });
                    _.forOwn(this._extend, (v, k) => {
                        if (!_.isString(v.relation)) {
                            Qs.push(M(v.table).fields(_.isFunction(v.fields) ? v.fields() : v.fields).where(v.where).where({ [v.fk]: array_columns(data, v.pk, true) }).select());
                        }
                        else {
                            Qs.push(R(v.relation).fields(_.isFunction(v.fields) ? v.fields() : v.fields).where(v.where).where({ [v.fk]: array_columns(data, v.pk, true) }).select());
                        }
                    });
                }
                return Q.all(Qs).then(result => {
                    let i = 1, data = result[0], one = {}, many = {}, extend = {}, config = {};
                    _.forOwn(this._one, (v, k) => {
                        one[v.name] = { values: result[i], config: v };
                        i++;
                    });
                    _.forOwn(this._many, (v, k) => {
                        many[v.name] = { values: result[i], config: v };
                        i++;
                    });
                    _.forOwn(this._extend, (v, k) => {
                        extend[v.name] = { values: result[i], config: v };
                        i++;
                    });
                    _.forOwn(data, (v, k) => {
                        _.forOwn(one, (d, f) => {
                            data[k][f] = _.filter(d.values, { [d.config.fk]: data[k][d.config.pk] })[0];
                        });
                        _.forOwn(many, (d, f) => {
                            data[k][f] = _.filter(d.values, { [d.config.fk]: data[k][d.config.pk] });
                        });
                        _.forOwn(extend, (d, f) => {
                            data[k] = _.assign(v, _.filter(d.values, { [d.config.fk]: data[k][d.config.pk] })[0]);
                        });
                        _.forOwn(this._foreach, (f, k) => {
                            if (_.isFunction(f)) {
                                f(data[k]);
                            }
                        });
                    });
                    return data;
                });
            });
        }
        else {
            return Q("Error");
        }
    }
    data_columns(arr, column) {
        let a = [];
        _.forOwn(arr, (v, k) => {
            a.push(v.dataValues[column]);
        });
        return _.uniq(a);
    }
    /**
     * 取出
     * @param arr
     * @returns {Array}
     */
    dataValues(arr, column = false) {
        let a = [];
        _.forOwn(arr, (v, k) => {
            if (false == column)
                a.push(v.dataValues);
            else
                a.push(v.dataValues[column]);
        });
        return a;
    }
    where(where) {
        this._model.where(where);
        return this;
    }
    load(DbConfig = null) {
        var conf = {};
        if (_.isString(DbConfig)) {
            //从定义目录加载
            conf = require(`${Config.MODEL_PATH}/${DbConfig}`);
        }
        else if (_.isObject(DbConfig)) {
        }
    }
    fields(fields) {
        this._fields = fields;
        return this;
    }
    selectAndCount() {
        return this._model.fields([this._pk]).selectAndCount().then(d => {
            if (d.rows.length == 0) {
                return [[], 0];
            }
            var PKs = array_columns(d.rows, this._pk);
            return Q.all([
                this.objects(PKs),
                d.count
            ]);
        });
    }
    order(order) {
        this._model.order(order);
        return this;
    }
    save(data) {
        return this._model.save(data).then(c => {
            return c > -1;
        }).catch(e => {
            return e;
        });
    }
    page(page, number) {
        this._model.page(page, number);
        return this;
    }
    add(data) {
        return this._model.add(data).then(d => {
            if (_.isObject(d) && d[this._pk] > 0) {
                return this.objects([d[this._pk]]).then(p => {
                    return p[0];
                });
            }
            else {
                return d;
            }
        });
    }
    // public find(){
    //     return this._model.find()
    // }
    del() {
        return this._model.del();
    }
    select() {
        return this._model.fields([this._pk]).select().then(d => {
            if (_.isArray(d) && d.length > 0) {
                var PKs = array_columns(d, this._pk);
                return this.objects(PKs);
            }
            else {
                return [];
            }
        });
    }
    /**
     * 查询一个
     */
    find() {
        return this._model.getFields(this._pk).then(d => {
            if (_.isNumber(d) && d > 0) {
                return this.objects([d]).then(data => {
                    return _.isArray(data) && data.length > 0 ? data[0] : {};
                });
            }
            else {
                return {};
            }
        });
    }
}
exports.Relation = Relation;
///<reference path="Config.ts"/>
const crypto = require('crypto');
const Sequelize = require('sequelize');
var fspath = "";
const Events = require('events');
const EM = new Events.EventEmitter();
const moment = require('moment');
const Q = require('q');
exports.Sequelize = Sequelize;
const Op = Sequelize.Op;
const debug = require('debug');
const md5 = require('md5');
var inited = false;
var db = null;
exports.Op = Op;
const _ = require('lodash');
exports._ = _;
const cluster = require('cluster');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const request = require('request');
// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ type: 'application/x-www-form-urlencoded', extended: false }));
// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));
// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }));
app.use(bodyParser.text({ type: 'text/plain' }));
const http = require('http').Server(app);
const io = require('socket.io')(http);
const fs = require('fs');
// if(fs.existsSync(global.APP_PATH+'/sessions')){
//     fs.mkdirSync('sessions');
// }
var host = "";
var port = "";
app.set('trust proxy', 1); // trust first proxy
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', `${req.headers.origin}`);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
};
app.use(allowCrossDomain);
exports.EM = EM;
express.app = app;
///<reference path="Start.ts"/>
///<reference path="Functions.ts"/>
class Server {
    constructor() {
        Server.init();
    }
    static controller(req, rep, next) {
        let C, A;
        C = req.params.C;
        A = req.params.A;
        let Controllers = require(`${Config.APP_PATH}/app/Controller.js`);
        if (Controllers[C] instanceof Function) {
        }
        else if (Controllers[C + 'Controller'] instanceof Function) {
            C = C + 'Controller';
        }
        else {
            C = 'Error';
            A = '_404';
        }
        try {
            let controller = new Controllers[C](req, rep);
            if (controller['_before_' + A] instanceof Function) {
                controller['_before_' + A].apply(controller, [req.body]);
                return;
            }
            if (controller[A] instanceof Function) {
                Q(controller[A].apply(controller, [req.body])).then(d => {
                    if (!_.isUndefined(d) && !_.isFunction(d))
                        controller.success(d);
                }).catch(e => {
                    controller.error(e);
                });
                // let p =controller[A].apply(controller,[req.body])
                // if(!!p&&typeof p.then=="function"){
                //     p.then(d=>{
                //         controller.success(d)
                //     }).catch(e=>{
                //         controller.error(e)
                //     })
                // }
            }
            else {
                Server.NotFound(req, rep, next);
            }
            if (controller['_after_' + A] instanceof Function) {
                controller['_after_' + A].apply(controller, [req.body]);
                return;
            }
        }
        catch (e) {
            Server.Error(e, req, rep, next);
            next();
        }
    }
    static ProxyPass(req, rep) {
        request.post(Config.PROXY_PASS + req.url, { form: Config.PROXY_ENCODE(req, rep) }, (err, response, body) => {
            let c = new Controller(req, rep);
            rep.send(Config[`${req.from}_ENCODE`].apply(c, [Config.PROXY_DECODE(response, body)]));
        });
    }
    static NotFound(req, rep, next) {
        if (Config.PROXY_PASS !== false) {
            this.ProxyPass(req, rep);
        }
        else {
        }
    }
}
Server.init = function () {
    if (!inited)
        Server.watcher();
    inited = true;
};
Server.watcher = function () {
    var chokidar = require('chokidar');
    chokidar.watch([
        Config.APP_PATH + '/app/*.js',
        Config.APP_PATH + '/Db/*.js',
        Config.APP_PATH + '/Conf/*.js',
    ])
        .on('add', Server.clean_cache)
        .on('change', Server.clean_cache)
        .on('unlink', Server.clean_cache);
};
Server.clean_cache = function (path) {
    if (require.cache[path]) {
        console.log("File Changed:" + path + " Reloaded");
        delete require.cache[path];
    }
};
Server.request = function (req, rep) {
    if (_.isUndefined(req.from)) {
        req.from = "HTTP";
    }
    req._config = {};
    req.config = function (key = null, $default = null) {
        let p = req._config;
        if (key === null) {
            return p;
        }
        let o = key.split('.');
        o.forEach(v => {
            if (_.isObject(p) && !_.isUndefined(p) && !_.isUndefined(p[v])) {
                p = p[v];
            }
            else {
                p = $default;
            }
        });
        return p;
    };
    // 读取host并加载对应的Config文件
    // let host = req.headers.host;
    Server.config(req, rep);
    // 检测是否是静态文件，如果是则直接读取并输出
    // if()
    if (_.isString(req.body) && '{' == req.body.substr(0, 1) && '}' == req.body.substr(req.body.length - 1)) {
        try {
            var body = JSON.parse(req.body);
            if (_.isObject(body)) {
                req.body = body;
            }
        }
        catch (e) {
        }
    }
    let match = req.originalUrl.match(/\/([A-Za-z][A-Za-z0-9]{0,})\/([A-Za-z][A-Za-z0-9]{0,})/);
    let C, A;
    if (req.query.i !== undefined && req.params.C === undefined) {
        let i = req.query.i !== undefined ? req.query.i.split('/') : ['Error', '_404'];
        if (i.length == 2) {
            C = i[0];
            A = i[1];
        }
        else {
            C = "Error";
            A = "_404";
        }
    }
    else if (match.length == 3) {
        C = match[1];
        A = match[2];
    }
    else {
        C = req.params.C;
        A = req.params.A;
    }
    req.query.i = `${C}/${A}`;
    req.params.C = C;
    req.params.A = A;
    req.post = function (key, $default = null) {
        let o = key.split('.');
        let p = req.body;
        o.forEach(v => {
            if (_.isObject(p) && !_.isUndefined(p) && !_.isUndefined(p[v])) {
                p = p[v];
            }
            else {
                p = $default;
            }
        });
        return p;
    };
    req.request = function (key = null, $default = null) {
        var getp = function (k, d) {
            let p = _.assign(req.query, req.body || {});
            if (key === null) {
                return p;
            }
            let o = k.split('.');
            o.forEach(v => {
                if (_.isObject(p) && !_.isUndefined(p) && !_.isUndefined(p[v])) {
                    p = p[v];
                }
                else {
                    p = $default;
                }
            });
            return p;
        };
        if (_.isString(key)) {
            return getp(key, $default);
        }
        else if (_.isObject(key)) {
            var Obj = {};
            _.forOwn(key, (v, k) => {
                Obj[k] = getp(k, v);
            });
            return Obj;
        }
        else if (_.isArray(key)) {
            var Obj = {};
            _.forOwn(key, (v, k) => {
                Obj[v] = getp(v, $default);
            });
            return Obj;
        }
        else {
            return $default;
        }
    };
};
Server.run = function () {
    Server.init();
    //加载session库
    const session = require('express-session');
    // var cookieParser = require('cookie-parser');
    //var Config = require(global.APP_PATH+'/config.js');
    var session_store;
    var session_path = Config.SESSION_PATH ? `${Config.SESSION_PATH}/${Config.APP_NAME}` : (`/tmp/${Config.APP_NAME}`);
    switch (Config.SESSION_HANDLER.toLowerCase()) {
        case "file":
            session_store = new (require('session-file-store')(session))({
                path: session_path
            });
            break;
        case "redis":
            session_store = new (require('connect-redis')(session))(Config.SESSION_REDIS);
            break;
        default:
            session_store = new (require('session-file-store')(session));
            // console.error('不支持的session方式')
            break;
    }
    app.use(session({
        secret: 'keyboard cat',
        saveUninitialized: true,
        resave: false,
        cookie: { maxAge: 60 * 60 * 1000 },
        store: session_store
    }));
    port = (process.env.VCAP_APP_PORT || process.env.PORT || Config.APP_PORT);
    host = (process.env.VCAP_APP_HOST || process.env.HOST || Config.APP_HOST);
    app.use(express.static(Config.PUBLIC_PATH, {
        index: ['index.html', 'index.htm']
    }));
    console.log('Attach Static Path:' + Config.PUBLIC_PATH);
    var numCPUs = require('os').cpus().lengthAdjust, num = 0;
    if (Config && Config.MAX_PROCESS) {
        num = Config.MAX_PROCESS > numCPUs ? numCPUs : Config.MAX_PROCESS;
    }
    if (num > 0) {
        if (cluster.isMaster) {
            // Fork workers
            console.log(`${Config.APP_NAME} is Starting..Please Waiting.`);
            for (var i = 0; i < num; i++) {
                cluster.fork();
            }
            cluster.on('exit', function (worker, code, signal) {
                console.log('worker ' + worker.process.pid + ' restart');
                setTimeout(function () { cluster.fork(); }, 2000);
            });
        }
        else {
            Server.Start();
        }
    }
    else {
        Server.Start();
    }
};
/**
 * 加载配置文件
 * @param host
 * @constructor
 */
Server.config = function (req, rep) {
    let host = req.headers.host;
    let ConfigPath = `${Config.APP_PATH}/Conf`;
    let Configs = [
        `${ConfigPath}/config.js`,
    ];
    if (Config.APP_DEBUG == true)
        Configs.push(`${ConfigPath}/debug.js`);
    Configs.push(`${ConfigPath}/${host}.js`);
    Configs.forEach(path => {
        if (fs.existsSync(path)) {
            try {
                var config = require(path);
                if (_.isObject(config)) {
                    req._config = Object.assign(req._config, config);
                }
            }
            catch (e) {
                debug(e);
            }
            finally {
            }
        }
    });
};
/**
 * 处理请求的方法
 * @param req
 * @param rep
 * @param next
 */
Server.onRequest = function (req, rep, next) {
    if (req.method == "OPTIONS") {
        rep.send("");
        next();
        return;
    }
    Server.request(req, rep);
    Server.controller(req, rep, next);
};
/**
 * WebSocket监听方法
 * @constructor
 */
Server.WebSocket = function () {
    io.on("authorization", function (hand, fn) {
        let parseCookie = require('cookie-parser');
        hand.cookie = parseCookie(hand.headers.cookie);
        let cid = hand.cookie['connect.sid'];
        if (cid) {
            session_store.get(cid, function (err, session) {
                if (err) {
                    fn(err.message, false);
                }
                else {
                    hand.session = session;
                    fn(null, true);
                }
            });
        }
        else {
            fn('nosession', false);
        }
    });
    io.on('connection', function (socket) {
        console.log('new Connection');
        // var session = socket.handshake.session;
        socket.on('message', function () {
        });
        socket.on('login', function () {
        });
        socket.on('logout', function () {
        });
        socket.on('disconnect', function () {
        });
    });
};
Server.Redis = function (config = {}) {
    let conf = Object.assign(Config.FROM_REDIS, config);
    var Redis = require('ioredis');
    var redis = new Redis(conf);
    var pub = new Redis(conf);
    redis.subscribe(conf.request, (err, count) => {
    });
    redis.on('message', (channel, message) => {
        console.log(`Redis From ${channel}: ${message}`);
        //此处解析成req和rep
        var body = Config.REDIS_DECODE(message, channel);
        var req = {
            query: {
                i: body.i
            },
            body: body.d,
            method: 'redis',
            path: channel,
            baseUrl: channel,
            params: {},
            from: 'REDIS'
        }, rep = {
            send: (data) => {
                pub.publish(conf.response, _.isString(data) ? data : JSON.stringify(data));
            }
        };
        Server.onRequest(req, rep, () => { });
    });
};
/**
 * 启动express
 * @constructor
 */
Server.Start = function () {
    process.on('uncaughtException', function (err) {
        console.log("Error", err);
    });
    app.use(require('morgan')(Config.DEBUG ? 'dev' : 'short'));
    let multer = require('multer');
    let storage = multer.diskStorage({
        destination: function (req, file, cb) {
            var dir = [Config.TMP_PATH, "Upload", moment().format('YYYY_MM_DD_h_mm')].join(path.sep);
            mkdirs(dir);
            cb(null, dir);
        }, filename: function (req, file, cb) {
            cb(null, [file.fieldname, file.originalname].join('_') + '.tmp');
        }
    });
    var upload = multer({ storage });
    app.use(upload.any());
    app.all('/*', Server.onRequest);
    app.use(Server.Error);
    console.log('Process Start Success:' + host + ":" + port);
    EM.emit('started', host, port);
    app.listen(port, host);
};
Server.Error = function (error, req, rep, next) {
    try {
        rep.send(Config[`${req.from}_ENCODE`].apply(new Controller(req, rep), [{}, 500, Config.APP_DEBUG ? error.stack : "SystemError"]));
    }
    catch (e) {
        console.log(e);
    }
};
exports.Server = Server;
//# sourceMappingURL=index.js.map