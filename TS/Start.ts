///<reference path="Config.ts"/>
const crypto = require('crypto');
const Sequelize = require('sequelize');
var fspath="";
const Events = require('events');
const EM = new Events.EventEmitter();
const moment = require('moment')
const Q = require('q')
exports.Sequelize = Sequelize;
const Op = Sequelize.Op;
const debug = require('debug');
const md5=require('md5');
var inited=false;
var db = null;
exports.Op = Op;
const _ = require('lodash')
exports._ = _;
const cluster = require('cluster');
const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const request = require('request');
// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/json' }))
app.use(bodyParser.urlencoded({ type: 'application/x-www-form-urlencoded' ,extended:false}))
// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))
// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }))
app.use(bodyParser.text({ type: 'text/plain' }))
const http=require('http').Server(app);
const io=require('socket.io')(http);
const fs = require('fs')
// if(fs.existsSync(global.APP_PATH+'/sessions')){
//     fs.mkdirSync('sessions');
// }
var host:string="";
var port:string|number="";
app.set('trust proxy', 1) // trust first proxy

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', `${req.headers.origin}`);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials','true');
    next();
};
app.use(allowCrossDomain);
declare let Controllers:any;
exports.EM = EM;
express.app=app;