declare const path: any;
/**
 *
 * @param Table
 * @returns Model
 * @constructor
 */
declare function M(Table: any): Model;
/**
 * 仿造php的array_columns函数
 * @param arr
 * @param column
 * @returns {Array}
 */
declare function array_columns(arr: any, column: any, unique?: boolean): any[];
/**
 * 取唯一值
 * @param arr
 */
declare function array_unique(arr: any): any;
/**
 *
 * @param name
 * @param {boolean} force
 * @returns Relation
 * @constructor
 */
declare function R(name: any, force?: boolean): any;
/**
 * 密码加密
 * @param pwd
 * @returns {string}
 */
declare function password_hash(pwd: any): string;
/**
 * 密码验证
 * @param pwd
 * @param sign
 * @returns {boolean}
 */
declare function password_verify(pwd: any, sign: any): boolean;
/**
 * 移动上传文件到指定地址
 * @param file
 * @param to
 */
declare var move_upload_file: (file: any, to: any) => void;
/**
 * 接受用户上传的所有文件，
 * @param req
 * @returns {any[]}
 */
declare var accept_upload_file: (req: any) => any[];
declare var mkdirs: (dirpath: any, mode?: number, callback?: any) => any;
declare class Config {
    /**
     * 是否开启转发，转发目标地址是什么
     * @type {boolean|string}
     */
    static PROXY_PASS: boolean;
    static PROXY_DECODE: (response: any, body: any) => any;
    static PROXY_ENCODE: (req: any, rep: any) => any;
    static SESSION_PATH: string;
    /**
     * 静态目录地址
     * @type {string}
     */
    static PUBLIC_PATH: string;
    /**
     * 是否开启调试模式
     * @type {boolean}
     */
    static DEBUG: boolean;
    /**
     * 是否启用日志，
     * @type {boolean}
     */
    static LOGER: boolean;
    /**
     * 来自Redis的请求
     * @type {{host: string; port: number; password: string; db: number; request: string; response: string}}
     */
    static FROM_REDIS: {
        host: string;
        port: number;
        password: string;
        db: number;
        request: string;
        response: string;
    };
    /**
     * 获取表的定义
     * @param ModelName
     * @returns {{}}
     */
    static getTableDefine(ModelName: any): {};
    /**
     * 获取表的主键名称
     * @param ModelName
     * @returns {string}
     */
    static getTablePK(ModelName: any): string;
    /**
     * 获取表字段列表
     * @param ModelName
     * @returns {any[]}
     */
    static getTableFields(ModelName: any): any[];
    static REDIS_DECODE: (data: any, channel: any) => {
        i: string;
        d: {};
        t: string;
        s: string;
    };
    static REDIS_ENCODE: (data: any, status?: number, error?: string) => {
        g: any;
        u: any;
        i: any;
        c: number;
        d: any;
        e: string;
    };
    static HTTP_DECODE: (data: any) => {
        i: string;
        d: {};
        t: string;
        s: string;
    };
    static HTTP_ENCODE: (data: any, status?: number, error?: string) => {
        g: any;
        u: any;
        i: any;
        c: number;
        d: any;
        e: string;
    };
    static WS_DECODE: (data: any) => {
        i: string;
        d: {};
        t: string;
        s: string;
    };
    static WS_ENCODE: (data: any, status?: number, error?: string) => {
        g: any;
        u: any;
        i: any;
        c: number;
        d: any;
        e: string;
    };
    /**
     * 模型定义路径
     * @type {string}
     */
    static MODEL_PATH: string;
    /**
     * 应用名称
     * @type {string}
     */
    static APP_NAME: string;
    /**
     * session处理方式
     * @type {string}
     */
    static SESSION_HANDLER: string;
    /**
     * 最大进程数
     * @type {number}
     */
    static MAX_PROCESS: number;
    /**
     * 应用目录
     * @type {string}
     */
    static APP_PATH: string;
    static TMP_PATH: string;
    static UPLOAD_PATH: string;
    static APP_DEBUG: boolean;
    /**
     * 监听端口
     * @type {number}
     */
    static APP_PORT: number;
    /**
     * 监听
     * @type {string}
     */
    static APP_HOST: string;
    /**
     * Redis配置
     * @type {{host: string; port: string; pass: string; db: number; ttl: number; logErrors: boolean}}
     */
    static SESSION_REDIS: {
        "host": string;
        "port": string;
        "pass": string;
        "db": number;
        "ttl": number;
        "logErrors": boolean;
    };
    /**
     * 数据库配置
     * @type {{DB_NAME: string; DB_USER: string; DB_PWD: string; DB_HOST: string; DB_PORT: string; DB_PREFIX: string}}
     */
    static DATABASE: {
        DB_NAME: string;
        DB_USER: string;
        DB_PWD: string;
        DB_HOST: string;
        DB_PORT: string;
        DB_PREFIX: string;
    };
}
declare class Controller {
    protected $model: String | boolean;
    /**
     *
     */
    protected $req: any;
    /**
     *
     */
    protected $rep: any;
    /**
     *
     */
    protected $C: string;
    /**
     *
     */
    protected $A: string;
    constructor(req: any, rep: any);
    protected _search: {
        Keyword: {
            table: string;
            fields: any[];
        };
        W: {
            table: string;
            fields: any[];
        };
    };
    /**
     * session操作
     * @param k
     * @param {any} v
     * @returns {any}
     */
    session(k: any, v?: any): any;
    /**
     * 发送数据
     * @param d
     */
    send(d: any): void;
    /**
     * cookie操作
     * @param k
     * @param v
     * @param {number} expire
     */
    cookie(k: any, v: any, expire?: number): void;
    /**
     * 返回成功
     * @param data
     */
    success(data: any): void;
    /**
     * 返回错误信息
     * @param tip
     * @param {number} c
     */
    error(tip: any, c?: number): void;
    /**
     * 获取对象
     * @param data
     */
    get(data: any): void;
    /**
     * 获取指定对象数据
     * @param data
     */
    gets(data: any): void;
    add(): void;
    /**
     * del方法
     */
    del(): void;
    save(): void;
    protected render(data: any, template?: boolean): void;
    search(): void;
}
declare class Model {
    private _db;
    private _model;
    private _true_table_name;
    private _fields;
    private _table_name;
    private _options;
    private _config;
    static parseWhere(where: any): {};
    config: {};
    $search: {};
    /**
     * 构造器
     * @param Table
     * @param {string} Prefix
     */
    constructor(Table: any, Prefix?: string);
    /**
     * 解析查询条件
     * @returns {{} | any}
     * @private
     */
    private _parse_where;
    /**
     * 获取查询字段
     * @returns {Array}
     * @private
     */
    private _parse_fields;
    private _parse_order();
    /**
     * 解析配置文件，生成查询属性
     * @returns {{}}
     * @private
     */
    private _parse_config;
    /**
     * 设置表的字段，默认读取所有的
     * @param fields
     */
    setTableFields: (fields: any) => void;
    define: (config?: {}) => {};
    /**
     * 获取一个Sequelize的模型
     * @returns {any}
     */
    getModel: () => any;
    object: () => void;
    /**
     * 检测是否存在并在不存在的情况下添加数据
     * @param data
     */
    addIfNotExist(data: any, where?: any): any;
    /**
     * 设定Where条件
     * @param where
     * @returns {Model.where}
     */
    where: (where: any) => any;
    /**
     * 设定字段列表，支持数组和字符串格式
     * @param {Number | String} fields
     * @param {boolean} exclude
     * @returns {Model.fields}
     */
    fields: (fields: String | Number, exclude?: boolean) => any;
    /**
     * 设定排序规则，
     * @param {String} order
     * @returns {Model.order}
     */
    order: (order: String) => any;
    /**
     * 发起查询请求
     * @returns {Bluebird<any[]>}
     */
    select: () => any;
    add: (data: any) => any;
    data: (data: any) => any;
    /**
     * 查找一个
     */
    find: () => any;
    /**
     * 批量添加数据
     * @param data
     * @returns {any}
     */
    addAll: (data: any) => any;
    /**
     * 取数量
     */
    count: () => void;
    /**
     * 支持selectAndCount
     * @returns {Promise<{count; rows: any[]}>}
     */
    selectAndCount: () => any;
    /**
     * 设置limit参数，
     * @param {number} Number
     */
    limit: (Number: number) => any;
    /**
     * 设置分页参数
     * @param {number} Page
     * @param {number} Number
     * @returns {Model.page}
     */
    page: (p: number, n: number) => any;
    /**
     * 调用delete语句
     * @returns {any}
     */
    del: () => any;
    /**
     * 调用save方法
     * @param data
     */
    save: (data: any) => any;
    /**
     * 执行自定义请求
     * @param sql
     * @returns {any}
     */
    query: (sql: any) => any;
    exec: (SQL: any, Type: any) => any;
    /**
     * 开启事物
     * @returns {IDBTransaction | any}
     */
    startTrans: () => any;
    /**
     * 提交
     */
    commit: () => void;
    /**
     * 回滚
     */
    rollback: () => void;
    /**
     * 清除查询条件，
     * @private
     */
    private _clean;
    /**
     * 获取某个字段
     * @param Fields
     * @param {boolean} More
     * @returns {any}
     */
    getFields(Fields: any, More?: boolean): any;
}
declare class Redis {
    private _sub;
    private _channels;
    private _redis;
    private _config;
    constructor(Host?: string, Port?: number, PWD?: any, DB?: number);
    /**
     * 发布
     * @param channel
     * @param message
     */
    publish: (channel: any, message: any) => void;
    /**
     * 订阅
     * @param channel
     * @param {Function} cb
     */
    subscribe: (channel: any) => void;
    /**
     * redis从获取请求数据
     * @param channel
     */
    command: (channel: any) => void;
}
/**
 * Created by castle on 2017-11-24.
 */
/**
 * 关系设定类
 */
declare class Relation {
    protected _one: any;
    protected _many: any;
    protected _extend: any;
    protected _table: string;
    protected _pk: string;
    protected _fields: Array<string>;
    protected _model: Model;
    protected _controller: any;
    protected _foreach: Array<Function>;
    /**
     *
     * @param Table 表名
     * @param Fields 字段列表
     * @param PK 主键
     */
    constructor(Table: any, Fields?: string | Array<string>, PK?: string);
    /**
     * 可以不通过构造函数来添加属性信息
     * @param Table
     * @param Fields
     * @param PK
     */
    config(Table: any, Fields: any, PK: any): void;
    /**
     * 拥有一个
     * @param name 关系名称
     * @param Table 表名称
     * @param Fields 要取的字段范围
     * @param TableField 子表字段
     * @param MainField 主表字段
     */
    hasOne(config: any): this;
    One: any;
    /**
     * 有多个配置
     * @param config
     * @returns {boolean}
     */
    hasMany(config: any): this;
    Many: any;
    /**
     * 扩展字段配置
     * @param config
     * @returns {boolean}
     */
    extend(config: any): this;
    /**
     *
     * @returns {any}
     * @constructor
     */
    /**
     *
     * @param config
     * @constructor
     */
    Extend: any;
    /**
     * 获取对象
     * @param {Array<Number>} PKValues
     * @returns {any}
     */
    objects(PKValues: Array<Number>): any;
    protected data_columns(arr: any, column: any): any;
    /**
     * 取出
     * @param arr
     * @returns {Array}
     */
    protected dataValues(arr: any, column?: any): any[];
    where(where: any): this;
    load(DbConfig?: {} | string): void;
    fields(fields: any): this;
    selectAndCount(): any;
    order(order: any): this;
    save(data: any): any;
    page(page: any, number: any): this;
    add(data: any): any;
    del(): any;
    select(): any;
    /**
     * 查询一个
     */
    find(): any;
}
declare const crypto: any;
declare const Sequelize: any;
declare var fspath: string;
declare const Events: any;
declare const EM: any;
declare const moment: any;
declare const Q: any;
declare const Op: any;
declare const debug: any;
declare const md5: any;
declare var inited: boolean;
declare var db: any;
declare const _: any;
declare const cluster: any;
declare const express: any;
declare const bodyParser: any;
declare const app: any;
declare const request: any;
declare const http: any;
declare const io: any;
declare const fs: any;
declare var host: string;
declare var port: string | number;
declare var allowCrossDomain: (req: any, res: any, next: any) => void;
declare let Controllers: any;
declare class Server {
    constructor();
    static init: () => void;
    static watcher: () => void;
    static clean_cache: (path: any) => void;
    static request: (req: any, rep: any) => void;
    static run: () => void;
    /**
     * 加载配置文件
     * @param host
     * @constructor
     */
    static config: (req: any, rep: any) => void;
    static controller(req: any, rep: any, next: any): void;
    /**
     * 处理请求的方法
     * @param req
     * @param rep
     * @param next
     */
    static onRequest: (req: any, rep: any, next: any) => void;
    static ProxyPass(req: any, rep: any): void;
    static NotFound(req: any, rep: any, next: any): void;
    /**
     * WebSocket监听方法
     * @constructor
     */
    static WebSocket: () => void;
    static Redis: (config?: {}) => void;
    /**
     * 启动express
     * @constructor
     */
    static Start: () => void;
    static Error: (error: any, req: any, rep: any, next: any) => void;
}
