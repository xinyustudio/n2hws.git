///<reference path="../index.d.ts"/>

var User = new Relation('User');
User.hasOne({
    name:'area',
    table:'area',
    fields:'AID,Title',
    pk:'AID',
    fk:'AID'
}).hasMany({
    name:'Log',
    table:'LoginLog',
    fields:'UID,IP',
    pk:'UID',
    fd:'UID'
}).extend({
    name:'extend',
    table:'UserExtend',
    fields:'UID,Name,Sex',
    pk:'UID',
    fd:'UID'
})
exports.User = User;