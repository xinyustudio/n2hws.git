var crypto = require('crypto')
/**
 * 密码加密
 * @param pwd
 * @returns {string}
 */
var password_hash=function (pwd):string {
    const md5 = crypto.createHash('md5');
    return md5.update(pwd).digest('hex').toLowerCase();
}
/**
 * 密码验证
 * @param pwd
 * @param sign
 * @returns {boolean}
 */
var password_verify=function (pwd,sign):boolean {
    const md5 = crypto.createHash('md5');
    return md5.update(pwd).digest('hex').toLowerCase()==sign;
}
var pwd=123
var hash = password_hash('123')
console.log(pwd,hash,password_verify(pwd,hash))