///<reference path="Functions.ts"/>
class Config{
    /**
     * 是否开启转发，转发目标地址是什么
     * @type {boolean|string}
     */
    public static PROXY_PASS=false;
    public static PROXY_DECODE = function (response,body) {
        return body;
    }
    public static PROXY_ENCODE = function(req,rep){
        return req.body;
    }

    public static SESSION_PATH = require('os').type()=="Windows_NT"?"./session":"/tmp"
    /**
     * 静态目录地址
     * @type {string}
     */
    public static PUBLIC_PATH=__dirname+'/Public';
    /**
     * 是否开启调试模式
     * @type {boolean}
     */
    public static DEBUG=true;
    /**
     * 是否启用日志，
     * @type {boolean}
     */
    public static LOGER=false;
    /**
     * 来自Redis的请求
     * @type {{host: string; port: number; password: string; db: number; request: string; response: string}}
     */
    public static FROM_REDIS={
        host:'127.0.0.1',
        port:6379,
        password:'',
        db:0,
        request:'request',//请求频道
        response:'response'//响应频道
    };

    /**
     * 获取表的定义
     * @param ModelName
     * @returns {{}}
     */
    public static getTableDefine(ModelName){
        var config ={};
        try{
            var s = require(Config.APP_PATH+`/Db/${ModelName}.js`)
            if(s){
                config=s[ModelName];
            }
        }catch (e){

        }finally {
            return config;
        }
    }

    /**
     * 获取表的主键名称
     * @param ModelName
     * @returns {string}
     */
    public static getTablePK(ModelName){
        var PK= 'ID';
        _.forOwn(Config.getTableDefine(ModelName),(v,k)=>{
            if(v.primaryKey===true){
                PK = k;
            }
        })
        return PK;
    }

    /**
     * 获取表字段列表
     * @param ModelName
     * @returns {any[]}
     */
    public static getTableFields(ModelName){
        var Fields= [];
        _.forOwn(Config.getTableDefine(ModelName),(v,k)=>{
            Fields.push(k)
        })
        return Fields;
    }
    public static REDIS_DECODE=function (data,channel) {
        var d={
            i:'Error/_404',
            d:{},
            t:'',
            s:''//session_id
        };
        try{
            if(_.isString(data)){
                data=JSON.parse(data);
                if(_.isObject(data)){
                    d.i=data.i||'Error/_404';
                    d.d=data.d;
                    d.t=data.t;
                    d.s=data.s||""
                }
            }else{

            }
        }catch (e){

        }
        return d;
    }
    public static REDIS_ENCODE=function (data,status=200,error="") {
        return {
            g:this.session?this.session('g')||[0]:[0],
            u:this.session?this.session('UID')||"":'',
            i:this.$req.query.i,
            c:status,
            d:data,
            e:error
        }
    }
    public static HTTP_DECODE=function (data) {

        var d={
            i:'Error/_404',
            d:{},
            t:'',
            s:''//session_id
        };
        try{
            if(_.isString(data)){
                data=JSON.parse(data);
                if(_.isObject(data)){
                    d.i=data.i||'Error/_404';
                    d.d=data.d;
                    d.t=data.t;
                    d.s=data.s||""
                }
            }else{

            }
        }catch (e){

        }
        return d;
    }
    public static HTTP_ENCODE=function (data,status=200,error="") {
        return {
            g:this.session?this.session('g')||[0]:[0],
            u:this.session?this.session('UID')||"":'',
            i:this.$req.query.i,
            c:status,
            d:data,
            e:error
        }
    }
    public static WS_DECODE=function (data) {

        var d={
            i:'Error/_404',
            d:{},
            t:'',
            s:''//session_id
        };
        try{
            if(_.isString(data)){
                data=JSON.parse(data);
                if(_.isObject(data)){
                    d.i=data.i||'Error/_404';
                    d.d=data.d;
                    d.t=data.t;
                    d.s=data.s||""
                }
            }else{

            }
        }catch (e){

        }
        return d;
    }
    public static WS_ENCODE=function (data,status=200,error="") {
        return {
            g:this.session?this.session('g')||[0]:[0],
            u:this.session?this.session('UID')||"":'',
            i:this.$req.query.i,
            c:status,
            d:data,
            e:error
        }
    }
    /**
     * 模型定义路径
     * @type {string}
     */
    public static MODEL_PATH:string="";
    /**
     * 应用名称
     * @type {string}
     */
    public static APP_NAME:string="N2hwsSimple";
    /**
     * session处理方式
     * @type {string}
     */
    public static SESSION_HANDLER:string="file";
    /**
     * 最大进程数
     * @type {number}
     */
    public static MAX_PROCESS:number=require('os').cpus().length;
    /**
     * 应用目录
     * @type {string}
     */
    public static APP_PATH:string=process.cwd();
    public static TMP_PATH:string=[process.cwd(),'Tmp'].join(path.sep);
    public static UPLOAD_PATH:string=[process.cwd(),'Upload'].join(path.sep);
    public static APP_DEBUG:boolean=false;
    /**
     * 监听端口
     * @type {number}
     */
    public static APP_PORT:number=18080;
    /**
     * 监听
     * @type {string}
     */
    public static APP_HOST:string="0.0.0.0";
    /**
     * Redis配置
     * @type {{host: string; port: string; pass: string; db: number; ttl: number; logErrors: boolean}}
     */
    public static SESSION_REDIS={
        "host" : "127.0.0.1",
        "port" : "6379",
        "pass" : "",
        "db" : 1,
        "ttl" : 1800,
        "logErrors" : true
    }
    /**
     * 数据库配置
     * @type {{DB_NAME: string; DB_USER: string; DB_PWD: string; DB_HOST: string; DB_PORT: string; DB_PREFIX: string}}
     */
    public static DATABASE={
        DB_NAME:"test",
        DB_USER:"root",
        DB_PWD:"123456",
        DB_HOST:"localhost",
        DB_PORT:"3306",
        DB_PREFIX:'prefix_',
    }
}
exports.Config = Config;